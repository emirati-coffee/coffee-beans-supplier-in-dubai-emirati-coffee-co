Emirati coffee is a big coffee wholesaler and supplier in UAE provide highest quality speciality coffee beans through direct trade with over 82 coffee producing countries, Emirati Coffee Dubai co-founder and CEO Mohamed Ali Al Madfai first opened the Emirati Coffee Co roastery in 2017 and the coffee shop followed a year later in 2018. Apart from coffee wholesalers, we offer a beautiful indoor or outdoor coffee cart with all equipment and highly skilled baristas to serve at your function or event 

It’s important to us to forge genuine connections with our suppliers, our customers and our community. We believe in doing our work in a way that is transparent and collaborative. Our thrill lies in the pursuit of mastering the creation of the perfect cup and sharing the knowledge that we’ve picked up along the way. Our Mission is to make specialty coffee accessible, affordable, and enjoyable for everyone

We believe in direct trade or relationship coffees. We believe in not just purchasing coffee from farmers, but cultivating a friendship that adds value, compassion, and integrity to our business as well as that of the supplier and farmer.
We understand that what makes coffee such a phenomenal beverage is the endless opportunities to discover something new in each cup. That’s why when we find coffee that we love we ensure we pay well above Fair Trade Prices to invest in the future of these farms.
Sustainability is at the heart of what we do, and industry education is the future of sustainability. We aim to connect farmers with roasters and baristas and bridge the gap between the start and finish of the coffee chain.

Let's connect to discuss how our dedication to sustainable sourcing, roasting, and delivering the world's finest coffees can elevate your coffee program from the conventional to the exceptional.

Phone: +97143395814
Email: info@emiraticoffee.com
Website: https://www.emiraticoffee.com/
Address: 14 9b, Building 1, Al Quoz Industrial Area Third, Dubai

https://ae.linkedin.com/company/emiraticoffee
https://www.facebook.com/emiraticoffee
https://www.instagram.com/emiraticoffee/
https://www.pinterest.com/emiraticoffee/
https://www.youtube.com/watch?v=2jQ2yw50iEc

Emirati coffee | Raw coffee company Dubai
Emirati coffee | احسن قهوة عربية في الامارات
